import React from 'react';

export default ({ youtubeId }) => (
  <div className="video">
    <iframe
      src={`https://www.youtube.com/embed/${youtubeId}`}
      frameBorder="0"
    />
  </div>
);
