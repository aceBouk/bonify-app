import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import Map from './views/Map.jsx';
import './styles/mainStyle.scss';

const App = () => (
  <BrowserRouter>
    <div>
      <Route exact path="/" component={Map}/>
    </div>
  </BrowserRouter>
);

ReactDOM.render((<App />), document.querySelector('#app') || document.createElement('div'));
